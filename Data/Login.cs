﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace BlazorTest.Data
{

    public class Password
    {
        public Password(string password)
        {
            _password = password;
            HasCapital = false;
            HasNumber = false;
            HasMinLength = false;
            HasSpecialChar = false;
        }

        private string _password { get; set; }
        public string Value { get { return _password; } set { _password = value; } }        

        #region validation
        public bool HasMinLength { get; private set; }
        public bool HasNumber { get; private set; }
        public bool HasCapital { get; private set; }
        public bool HasSpecialChar { get; private set; }

        public string CssHasMinLength => HasMinLength ? "icon-validation-ok" : "icon-validation-invalid";
        public string CssHasNumber => HasNumber ? "icon-validation-ok" : "icon-validation-invalid";
        public string CssHasCapital => HasCapital ? "icon-validation-ok" : "icon-validation-invalid";
        public string CssHasSpecialChar => HasSpecialChar ? "icon-validation-ok" : "icon-validation-invalid";

        public bool IsValid()
        {
            if (HasSpecialChar && HasNumber && HasCapital && HasMinLength)
                return true;
            else
                return false;
        }

        public bool Validate()
        {
            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMinimum8Chars = new Regex(@".{8,}");
            var hasSpecialChar = new Regex(@"[^A-Za-z0-9]");

            HasMinLength = hasMinimum8Chars.IsMatch(_password);
            HasNumber = hasNumber.IsMatch(_password);
            HasCapital = hasUpperChar.IsMatch(_password);
            HasSpecialChar = hasSpecialChar.IsMatch(_password);

            return IsValid();
        }
        #endregion
    }

    public class Login
    {
        public Password Password { get; set; }
        public string ConfirmPassword { get; set; }

        public bool ValidationRepeatPassword => Password.Value == ConfirmPassword;
        public string ValidationMessageRepeatPassword => !ValidationRepeatPassword ? "Repeat Password not valid" : string.Empty;
    }
}
