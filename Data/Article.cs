﻿namespace BlazorTest.Data
{
    public class Article
    {
        public Article(int id, string title, string subtitle, string content, string imageUrl, string link)
        {
            Id = id;
            Title = title;
            Subtitle = subtitle;
            Content = content;
            ImageUrl = imageUrl;
            Link = link;
        }

        public int Id { get; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Content { get; set; }
        public string ImageUrl { get; set; }
        public string Link { get; set; }
    }
}
